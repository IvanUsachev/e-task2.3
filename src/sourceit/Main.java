package sourceit;

import java.util.Scanner;


public class Main {

    public static void main(String[] args) {
        // write your code here
        Bank[] banks = Generator.generate();

        //init scanner
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите название банка: " +
                "(ОАО Приват Банк - Приват; " +
                "ГБ Ощад-БАнк - Ощад; " +
                "НБ ПУМБ - ПумБ;):");
        String nameBank1 = scan.nextLine();
        System.out.println("Введите сумму для обмена (грн.):");
        int money = scan.nextInt();
        System.out.println("Введите валюту на котрую будем менять" +
                "(Долары - USD; " +
                "Евро - EUR; " +
                "Фунты - FUNT;):");
        String kursBank = scan.next();
        System.out.println("Введите операцию для выполнеия " +
                "(Покупка - Buy; " +
                "Продажа - Sell;):");
        String buySell = scan.next();

        for (Bank bank : banks) {
            if (bank.nameBank.equalsIgnoreCase(nameBank1) && kursBank.equalsIgnoreCase("USD") && buySell.equalsIgnoreCase("Buy")) {
                System.out.println("Выйдет: " + money / bank.kursBuyUSD + " долларов");
            } else if (bank.nameBank.equalsIgnoreCase(nameBank1) && kursBank.equalsIgnoreCase("USD") && buySell.equalsIgnoreCase("Sell")) {
                System.out.println("Выйдет: " + money / bank.kursSellUSD + " долларов");
            } else if (bank.nameBank.equalsIgnoreCase(nameBank1) && kursBank.equalsIgnoreCase("EUR") && buySell.equalsIgnoreCase("Buy")) {
                System.out.println("Выйдет: " + money / bank.kursBuyEUR + " евро");
            } else if (bank.nameBank.equalsIgnoreCase(nameBank1) && kursBank.equalsIgnoreCase("EUR") && buySell.equalsIgnoreCase("Sell")) {
                System.out.println("Выйдет: " + money / bank.kursBuyEUR + " евро");
            } else if (bank.nameBank.equalsIgnoreCase(nameBank1) && kursBank.equalsIgnoreCase("FUNT") && buySell.equalsIgnoreCase("Buy")) {
                System.out.println("Выйдет: " + money / bank.kursBuyFunt + " фунтов");
            } else if (bank.nameBank.equalsIgnoreCase(nameBank1) && kursBank.equalsIgnoreCase("FUNT") && buySell.equalsIgnoreCase("Sell")) {
                System.out.println("Выйдет: " + money / bank.kursBuyFunt + " фунтов");
            } else {
                System.err.println();
            }

        }


    }
}



